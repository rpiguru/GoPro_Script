import time
import socket
import json

import sys
import urllib.request

KEEP_ALIVE_PERIOD = 2500
KEEP_ALIVE_CMD = 2
UDP_IP = "10.5.5.9"
UDP_PORT = 8554


def get_command_msg():
    return "_GPHD_:%u:%u:%d:%1lf\n" % (0, 0, 2, 0)


response_raw = urllib.request.urlopen('http://10.5.5.9/gp/gpControl').read().decode('utf-8')
jsondata = json.loads(response_raw)
response = jsondata["info"]["firmware_version"]
print(jsondata["info"]["model_name"] + "\n" + jsondata["info"]["firmware_version"])
print("Enable Preview...")
r = urllib.request.urlopen("http://10.5.5.9/gp/gpControl/execute?p1=gpStream&a1=proto_v2&c1=restart").read()
print('Response: {}'.format(r))

print('Checking status of Camera')
connected_status = False
s_time = time.time()
while not connected_status:
    req = urllib.request.urlopen("http://10.5.5.9/gp/gpControl/status")
    data = req.read()
    encoding = req.info().get_content_charset('utf-8')
    json_data = json.loads(data.decode(encoding))
    print('Status: {}'.format(json_data['status']['31']))
    if json_data["status"]["31"] >= 1:
        break
    if time.time() - s_time > 60:
        print('Failed to get correct status from H4, exiting...')
        sys.exit(1)


while True:
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(get_command_msg().encode(), (UDP_IP, UDP_PORT))
    time.sleep(KEEP_ALIVE_PERIOD / 1000)
