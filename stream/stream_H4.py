"""
Python script for streaming live video from GoPro H4
"""
import time
import sys
import socket
import subprocess
import signal
import json

if not sys.version_info >= (3, 0):
    sys.stdout.write("Sorry, requires Python 3.x, not Python 2.x. Please execute with `python3 stream_H4.py`\n")
    sys.exit(1)

import urllib.request


SOURCE_URL = "udp://10.5.5.9:8554"
STREAM_ID = "fufy-gqa6-za53-3h4b"
KEEP_ALIVE_PERIOD = 2500
KEEP_ALIVE_CMD = 2
UDP_IP = "10.5.5.9"
UDP_PORT = 8554


def get_command_msg():
    return "_GPHD_:%u:%u:%d:%1lf\n" % (0, 0, 2, 0)


def gopro_live():

    response_raw = urllib.request.urlopen('http://10.5.5.9/gp/gpControl').read().decode('utf-8')
    jsondata = json.loads(response_raw)
    response = jsondata["info"]["firmware_version"]
    if "HD4" in response or "HD3.2" in response or "HD5" in response or "HX" in response or "HD6" in response:
        print(jsondata["info"]["model_name"] + "\n" + jsondata["info"]["firmware_version"])
        # HTTP GETs the URL that tells the GoPro to start streaming.
        print("Enable Preview...")
        r = urllib.request.urlopen("http://10.5.5.9/gp/gpControl/execute?p1=gpStream&a1=proto_v2&c1=restart").read()
        print('Response: {}'.format(r))

        # print('Sending `PREVIEW` command...')
        # r = urllib.request.urlopen("http://10.5.5.9/gp/gpControl/command/shutter?p=1").read()
        # print('Response: {}'.format(r))

        # GoPro HERO4 Session needs status 31 to be greater or equal than 1 in order to start the live feed.
        print('Checking status of Camera')
        connected_status = False
        s_time = time.time()
        while not connected_status:
            req = urllib.request.urlopen("http://10.5.5.9/gp/gpControl/status")
            data = req.read()
            encoding = req.info().get_content_charset('utf-8')
            json_data = json.loads(data.decode(encoding))
            print('Status: {}'.format(json_data))
            if json_data["status"]["31"] >= 1:
                break
            if time.time() - s_time > 60:
                print('Failed to get correct status from H4, exiting...')
                sys.exit(1)

        cmd = "cvlc \"udp://:8554\" -Idummy --network-caching 4000 --sout '#transcode{vcodec=FLV1,acodec=mp3," \
              "samplerate=11025,threads=2,fps=25}:std{access=rtmp,mux=ffmpeg{mux=flv}," \
              "dst=rtmp://a.rtmp.youtube.com/live2/%s'" % STREAM_ID
        print('Starting streaming = {}'.format(cmd))
        subprocess.Popen(cmd, shell=True)
        while True:
            sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            sock.sendto(get_command_msg().encode(), (UDP_IP, UDP_PORT))
            time.sleep(KEEP_ALIVE_PERIOD / 1000)
    else:
        print('Invalid Firmware info({}), exiting...'.format(response))
        sys.exit(1)


def quit_gopro(signal, frame):
    urllib.request.urlopen("http://10.5.5.9/gp/gpControl/command/shutter?p=0").read()
    sys.exit(0)


if __name__ == '__main__':

    signal.signal(signal.SIGINT, quit_gopro)
    gopro_live()
